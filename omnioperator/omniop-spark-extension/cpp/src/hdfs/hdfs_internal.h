//
// Created by l00451143 on 2023/11/27.
//

#ifndef SPARK_THESTRAL_PLUGIN_HDFS_INTERNAL_H
#define SPARK_THESTRAL_PLUGIN_HDFS_INTERNAL_H

#endif //SPARK_THESTRAL_PLUGIN_HDFS_INTERNAL_H

#include "include/hdfs.h"
#include "status.h"

namespace orc {

class LibHdfsShim {
public:
    LibHdfsShim();
    ~LibHdfsShim();

    // return hdfsFS
    StatusCode Connect(const char* url, tPort port);
    // return hdfsFile
    StatusCode OpenFile(const char* path, int bufferSize, short replication, int32_t blocksize);
    // return tSize
    int32_t Read( void* buffer, int32_t length, int64_t offset);

    int GetFileSize(const char* path);

private:
    hdfsFS fs_;
    hdfsFile file_;

    int CloseFile();

    int Disconnect();
};

}