//
// Created by l00451143 on 2023/11/27.
//

#ifndef SPARK_THESTRAL_PLUGIN_STATUS_H
#define SPARK_THESTRAL_PLUGIN_STATUS_H

#endif //SPARK_THESTRAL_PLUGIN_STATUS_H
namespace orc {

    enum StatusCode : char {
        OK = 0,
        FSConnectError = 1,
        OpenFileError = 2,
        ReadFileError = 3,
        InfoFileError = 4
    };
    class Status {
    public:
        static bool ok(StatusCode code) { return code == OK; }
    };
}