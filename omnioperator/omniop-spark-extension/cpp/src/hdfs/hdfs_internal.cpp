//
// Created by l00451143 on 2023/11/27.
//

#include "hdfs_internal.h"
#include <iostream>

using namespace orc;

LibHdfsShim::LibHdfsShim() {
    // std::cout << "Create to new hdfs filesystem"<< std::endl;
}

LibHdfsShim::~LibHdfsShim() {
    // std::cout << "Begin to release hdfs filesystem"<< std::endl;
    if (fs_ != nullptr){
        this->Disconnect();
    }
    if (fs_ != nullptr && file_ != nullptr){
        this->CloseFile();
    }
    // std::cout << "End to release hdfs filesystem"<< std::endl;
}

StatusCode LibHdfsShim::Connect(const char *url, tPort port) {
    // std::string urlStr(url);
    // std::cout << "url: " << urlStr << ", port: " << port << std::endl;
    this->fs_= hdfsConnect(url, port);
    if (!fs_) {
        // std::cout << "Fail to connect filesystem"<< std::endl;
        return StatusCode::FSConnectError;
    }
    return StatusCode::OK;
}

StatusCode LibHdfsShim::OpenFile(const char *path, int bufferSize, short replication,
                                 int32_t blocksize) {
    // std::string pathStr(path);
    // std::cout << "path: " << pathStr << ", bufferSize: " << bufferSize << ", replication: " << replication << ", blocksize: " << blocksize << std::endl;
    this->file_ = hdfsOpenFile(this->fs_, path, O_RDONLY, bufferSize, replication, blocksize);
    if (!file_) {
        // std::cout << "Fail to open file"<< std::endl;
        this->Disconnect();
        return StatusCode::OpenFileError;
    }
    return StatusCode::OK;
}

int LibHdfsShim::GetFileSize(const char *path) {
    // std::string pathStr(path);
    // std::cout << "path: " << pathStr << std::endl;
    hdfsFileInfo* fileInfo = hdfsGetPathInfo(this->fs_, path);
    if (!fileInfo){
        std::cout << "Fail to get path info"<< std::endl;
    }else{
        // std::string fileName(fileInfo->mName);
        // std::cout << "Success get path info, size: " << fileInfo->mSize << ", fileName: " << fileName << std::endl;
    }
    return fileInfo->mSize;
}

int32_t LibHdfsShim::Read(void *buffer, int32_t length, int64_t offset) {
    return hdfsPread(this->fs_, this->file_, offset, buffer, length);
}

int LibHdfsShim::CloseFile() {
     // std::cout << "Close hdfs filesystem"<< std::endl;
    return hdfsCloseFile(this->fs_, this->file_);
}

int LibHdfsShim::Disconnect() {
    // std::cout << "Disconnect hdfs filesystem"<< std::endl;
    return hdfsDisconnect(this->fs_);
}
