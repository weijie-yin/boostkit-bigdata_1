//
// Created by l00451143 on 2023/11/27.
//

#include <memory>

#include <iostream>
#include "OrcFileRewrite.hh"
#include "hdfs/hdfs_internal.h"


#include "OrcFileRewrite.hh"

namespace orc {

    class HdfsFileInputStreamV2 : public InputStream {
    private:
        std::string filepath_;
        uint64_t total_length_;
        const uint64_t READ_SIZE = 1024 * 1024; //1 MB

        std::unique_ptr<LibHdfsShim> file_system_;

    public:
        HdfsFileInputStreamV2(std::string path) {
            // std::cout << "Begin to create hdfs input steam"<< std::endl;
            this->file_system_ = std::make_unique<LibHdfsShim>();

            hdfs::URI uri;
            try {
                uri = hdfs::URI::parse_from_string(path);
            } catch (const hdfs::uri_parse_error&) {
                throw ParseError("Malformed URI: " + path);
            }
            // std::cout << "Success to parse uri, host: " << uri.get_host().c_str()
            //        << ", port: " << uri.get_port()
            //        << ", file path: " << uri.get_path()
            //        << std::endl;

            this->filepath_ = uri.get_path();

            StatusCode fs_status = file_system_->Connect(uri.get_host().c_str(),static_cast<uint16_t>(uri.get_port()));
            if (fs_status != OK){
                throw ParseError("URI: " + path + ", fail to connect filesystem.");
            }
            // std::cout << "Success to connect hdfs file system"<< std::endl;

            StatusCode file_status = file_system_->OpenFile(filepath_.c_str(), 0, 0, 0);
            if (file_status != OK){
                throw ParseError("file path: " + filepath_ + ", fail to connect filesystem.");
            }
            // std::cout << "Success to connect open hdfs file"<< std::endl;

            this->total_length_ = file_system_->GetFileSize(filepath_.c_str());
            // std::cout << "end to create hdfs input steam, total_length_: " << total_length_ << std::endl;
        }

        ~HdfsFileInputStreamV2() override {
        }

        uint64_t getLength() const override {
            return this->total_length_;
        }

        uint64_t getNaturalReadSize() const override {
            return this->READ_SIZE;
        }

        const std::string& getName() const override {
            return filepath_;
        }

        void read(void* buf,
                  uint64_t length,
                  uint64_t offset) override {
            if (!buf) {
                throw ParseError("Buffer is null");
            }

            // std::cout << "hdfs file input stream, begin read, length: " << length << ", offset: " << offset << std::endl;

            char* buf_ptr = reinterpret_cast<char*>(buf);
            int32_t total_bytes_read = 0;
            int32_t last_bytes_read = 0;

            do{
                last_bytes_read = file_system_->Read(buf_ptr, length - total_bytes_read, offset + total_bytes_read);
                if (last_bytes_read < 0) {
                    // std::cout << "Fail to get read file, read bytes: " << last_bytes_read << std::endl;
                    throw ParseError("Error reading bytes the file.");
                }
                total_bytes_read += last_bytes_read;
                buf_ptr += last_bytes_read;
                // std::cout << "read hdfs, total_bytes_read: " << total_bytes_read << ", last_bytes_read: " << last_bytes_read << ", buf_ptr: " << buf_ptr << std::endl;
            } while (total_bytes_read < length);
            // std::cout << "hdfs file input stream, end read, total_bytes_read: " << total_bytes_read << ", last_bytes_read: " << last_bytes_read << std::endl;
        }
    };

    std::unique_ptr<InputStream> readHdfsFileRewrite(const std::string& path, std::vector<hdfs::Token*>& tokens) {
        return std::unique_ptr<InputStream>(new HdfsFileInputStreamV2(path));
    }
}